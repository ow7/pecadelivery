from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

from peca_delivery.shop.models import STATE_CHOICES


class User(AbstractUser):
    pass


class Profile(models.Model):
    user = models.OneToOneField(
        "User", verbose_name=_("Usuário"), on_delete=models.CASCADE, default=1
    )
    avatar = models.ImageField(_("Avatar"), upload_to="profile/avatar", blank=False)
    phone = models.CharField(_("Fone"), max_length=20)
    birth_date = models.DateField(_("Aniversário"), null=True, blank=True)
    bio = models.TextField(_("Biografia"), max_length=500, blank=True)
    email_confirmed = models.BooleanField(_("Email Confirmado?"), default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["user"]
        verbose_name = _("Perfil")
        verbose_name_plural = _("Perfis")
        db_table = "user_profile"

    def __str__(self):
        return self.user.get_full_name()


class Address(models.Model):
    user = models.OneToOneField(
        "User", verbose_name=_("Usuário"), on_delete=models.CASCADE, default=1
    )
    name = models.CharField(_("Nome do Endereço"), max_length=100)
    address = models.CharField(_("Endereço"), max_length=200)
    address2 = models.CharField(_("Complemento"), max_length=200, blank=True, null=True)
    postal_code = models.CharField(_("CEP"), max_length=20)
    city = models.CharField(_("Cidade"), max_length=100)
    state = models.CharField(_("UF"), max_length=2, choices=STATE_CHOICES, default="PE")
    delivery_address = models.BooleanField(_("Endereço de Entrega?"), default=False)

    class Meta:
        ordering = ["user"]
        verbose_name = _("Endereço")
        verbose_name_plural = _("Endereços")
        db_table = "user_address"

    def __str__(self):
        return "{0} - {1}".format(self.user.get_full_name(), self.name)
