from django.apps import AppConfig


class PaymentConfig(AppConfig):
    name = "peca_delivery.payment"
    verbose_name = "Pagamentos"

    def ready(self):
        # import signal handlers
        import peca_delivery.payment.signals
