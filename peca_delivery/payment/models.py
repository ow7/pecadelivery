from django.db import models
from django.utils.translation import ugettext_lazy as _


CARD_TYPE = (
    (0, "CRÉDITO"),
    (1, "DÉBITO"),
)


class PaymentMethod(models.Model):
    image = models.ImageField(_("Bandeira"), upload_to="payment/flags", blank=True)
    name = models.CharField(_("Nome"), max_length=20)
    card_type = models.IntegerField(
        _("Tipo de cartão"), choices=CARD_TYPE, null=True, blank=True
    )

    def __str__(self):
        if not self.card_type == None:
            return ("{} ({})").format(self.name, self.get_card_type_display())
        else:
            return self.name

    class Meta:
        ordering = ["name"]
        verbose_name = _("Forma de Pagamento")
        verbose_name_plural = _("Formas de Pagamento")
        db_table = "payment_method"
