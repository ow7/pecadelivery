from django.contrib import admin

from .models import Client, Domain


class ClientAdmin(admin.ModelAdmin):
    list_per_page = 100
    list_display = ["name"]
    ordering = ["name"]
    # actions = [drop_schema, create_admin]


class DomainAdmin(admin.ModelAdmin):
    list_per_page = 100
    list_display = ["domain"]
    ordering = ["domain"]
    # actions = [drop_schema, create_admin]


admin.site.register(Client, ClientAdmin)
admin.site.register(Domain, DomainAdmin)
