from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_tenants.models import TenantMixin, DomainMixin


class Client(TenantMixin):
    name = models.CharField(max_length=100)
    paid_until = models.DateField()
    on_trial = models.BooleanField()
    created_on = models.DateField(auto_now_add=True)
    logo = models.ImageField(
        _("Logo"), upload_to="customers/clients", blank=True, null=True
    )

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

    class Meta:
        ordering = ["name"]
        verbose_name = _("Cliente")
        verbose_name_plural = _("Clientes")
        db_table = "client"

    def __str__(self):
        return self.name


class Domain(DomainMixin):
    pass

    def __str__(self):
        return self.domain
