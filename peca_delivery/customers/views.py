from django.shortcuts import render

from .models import Client


def client_list(request):
    client = Client.objects.all().exclude(schema_name="public")
    context = {"clients": client}

    return render(request, "customers/list.html", context)
