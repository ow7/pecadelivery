from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    # url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path("admin/", admin.site.urls),
    path("cart/", include("peca_delivery.cart.urls", namespace="cart")),
    path("customers/", include("peca_delivery.customers.urls", namespace="customers")),
    path(
        "<slug:store_slug>/orders/",
        include("peca_delivery.orders.urls", namespace="orders"),
    ),
    path(
        "<slug:store_slug>/payment/",
        include("peca_delivery.payment.urls", namespace="payment"),
    ),
    path("<slug:store_slug>/app/", include("peca_delivery.app.urls", namespace="app"),),
    path("paypal/", include("paypal.standard.ipn.urls")),
    # auth
    path("entrar/", LoginView.as_view(), name="sign-in",),
    path("sair/", LogoutView.as_view(), {"next_page": "/"}, name="sign-out"),
    path("", include("peca_delivery.shop.urls", namespace="shop")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if hasattr(settings, "ADMIN_SITE_HEADER"):
    admin.site.site_header = settings.ADMIN_SITE_HEADER
    admin.site.site_title = settings.ADMIN_SITE_HEADER


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls)),] + urlpatterns
