from django.contrib import admin

from .models import Store, Category, Product, Banner, Schedule


class StoreAdmin(admin.ModelAdmin):
    list_display = ["name"]
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Store, StoreAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["store", "name"]
    list_display_links = ["store", "name"]
    list_filter = ["store"]
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ["admin_image", "name", "unity", "price", "status", "store"]
    list_display_links = ["store", "admin_image", "name"]
    list_filter = ["store", "created", "updated"]
    list_editable = ["price", "status"]
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Product, ProductAdmin)


class BannerAdmin(admin.ModelAdmin):
    list_display = ["store", "title", "admin_image"]
    list_display_links = ["store", "title", "admin_image"]
    list_filter = ["store"]


admin.site.register(Banner, BannerAdmin)


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ["store", "display_day_hour"]
    list_display_links = ["store", "display_day_hour"]
    list_filter = ["store"]


admin.site.register(Schedule, ScheduleAdmin)
