from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = "peca_delivery.shop"
    verbose_name = "Loja"
