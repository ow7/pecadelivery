from django.urls import path

from . import views

app_name = "shop"
urlpatterns = [
    path("", views.store_list, name="store_list"),
    path("<slug:store_slug>/", views.product_list, name="product_list"),
    # path(r'^(?P<store_slug>[-\w]+)/$',
    #     views.product_list,
    #     name='product_list_by_store'),
    path(
        "<slug:store_slug>/<slug:category_slug>/",
        views.product_list,
        name="product_list_by_category",
    ),
    # path(r'^(?P<product_id>\d+)/(?P<slug>[-\w]+)/$',
    #     views.product_detail,
    #     name='product_detail')
]
