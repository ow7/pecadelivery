from django.db import models
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

# from django.core.files import File
# from django.core.files.temp import NamedTemporaryFile

# from urllib.request import urlopen

from peca_delivery.customers.models import Client


STATE_CHOICES = (
    ("AC", "Acre"),
    ("AL", "Alagoas"),
    ("AP", "Amapá"),
    ("AM", "Amazonas"),
    ("BA", "Bahia"),
    ("CE", "Ceará"),
    ("DF", "Distrito Federal"),
    ("ES", "Espírito Santo"),
    ("GO", "Goiás"),
    ("MA", "Maranhão"),
    ("MT", "Mato Grosso"),
    ("MS", "Mato Grosso do Sul"),
    ("MG", "Minas Gerais"),
    ("PA", "Pará"),
    ("PB", "Paraíba"),
    ("PR", "Paraná"),
    ("PE", "Pernambuco"),
    ("PI", "Piauí"),
    ("RJ", "Rio de Janeiro"),
    ("RN", "Rio Grande do Norte"),
    ("RS", "Rio Grande do Sul"),
    ("RO", "Rondônia"),
    ("RR", "Roraima"),
    ("SC", "Santa Catarina"),
    ("SP", "São Paulo"),
    ("SE", "Sergipe"),
    ("TO", "Tocantins"),
)

PRODUCT_STATUS = (
    (0, _("Ativo")),
    (1, _("Em Falta")),
    (2, _("Ocultar")),
)

WEEK_DAY_CHOICES = (
    (0, _("Domingo")),
    (1, _("Segunda")),
    (2, _("Terça")),
    (3, _("Quarta")),
    (4, _("Quinta")),
    (5, _("Sexta")),
    (6, _("Sabádo")),
)

UNITY_CHOICES = (
    ("AMPOLA", _("AMPOLA")),
    ("BALDE", _("BALDE")),
    ("BANDEJ", _("BANDEJA")),
    ("BARRA", _("BARRA")),
    ("BISNAG", _("BISNAGA")),
    ("BLOCO", _("BLOCO")),
    ("BOBINA", _("BOBINA")),
    ("BOMB", _("BOMBONA")),
    ("CAPS", _("CAPSULA")),
    ("CART", _("CARTELA")),
    ("CENTO", _("CENTO")),
    ("CJ", _("CONJUNTO")),
    ("CM", _("CENTIMETRO")),
    ("CM2", _("CENTIMETRO QUADRADO")),
    ("CX", _("CAIXA")),
    ("CX2", _("CAIXA COM 2 UNIDADES")),
    ("CX3", _("CAIXA COM 3 UNIDADES")),
    ("CX5", _("CAIXA COM 5 UNIDADES")),
    ("CX10", _("CAIXA COM 10 UNIDADES")),
    ("CX15", _("CAIXA COM 15 UNIDADES")),
    ("CX20", _("CAIXA COM 20 UNIDADES")),
    ("CX25", _("CAIXA COM 25 UNIDADES")),
    ("CX50", _("CAIXA COM 50 UNIDADES")),
    ("CX100", _("CAIXA COM 100 UNIDADES")),
    ("DISP", _("DISPLAY")),
    ("DUZIA", _("DUZIA")),
    ("EMBAL", _("EMBALAGEM")),
    ("FARDO", _("FARDO")),
    ("FOLHA", _("FOLHA")),
    ("FRASCO", _("FRASCO")),
    ("GALAO", _("GALÃO")),
    ("GF", _("GARRAFA")),
    ("GRAMAS", _("GRAMAS")),
    ("JOGO", _("JOGO")),
    ("KG", _("QUILOGRAMA")),
    ("KIT", _("KIT")),
    ("LATA", _("LATA")),
    ("LITRO", _("LITRO")),
    ("M", _("METRO")),
    ("M2", _("METRO QUADRADO")),
    ("M3", _("METRO CÚBICO")),
    ("MILHEI", _("MILHEIRO")),
    ("ML", _("MILILITRO")),
    ("MWH", _("MEGAWATT HORA")),
    ("PACOTE", _("PACOTE")),
    ("PALETE", _("PALETE")),
    ("PARES", _("PARES")),
    ("PC", _("PEÇA")),
    ("POTE", _("POTE")),
    ("K", _("QUILATE")),
    ("RESMA", _("RESMA")),
    ("ROLO", _("ROLO")),
    ("SACO", _("SACO")),
    ("SACOLA", _("SACOLA")),
    ("TAMBOR", _("TAMBOR")),
    ("TANQUE", _("TANQUE")),
    ("TON", _("TONELADA")),
    ("TUBO", _("TUBO")),
    ("UNID", _("UNIDADE")),
    ("VASIL", _("VASILHAME")),
    ("VIDRO", _("VIDRO")),
)


class Store(models.Model):
    client = models.ForeignKey(
        Client, verbose_name=_("Cliente"), on_delete=models.CASCADE, null=True,
    )
    logo = models.ImageField(_("Logo"), upload_to="shop/stores", blank=False)
    name = models.CharField(_("Nome da Loja"), max_length=100, db_index=True)
    slug = models.SlugField(_("URL"), max_length=100, db_index=True, unique=True)
    phone = models.CharField(_("Fone"), max_length=20)
    address = models.CharField(_("Endereço"), max_length=200)
    address2 = models.CharField(_("Complemento"), max_length=200, blank=True, null=True)
    postal_code = models.CharField(_("CEP"), max_length=20)
    city = models.CharField(_("Cidade"), max_length=100)
    state = models.CharField(_("UF"), max_length=2, choices=STATE_CHOICES, default="PE")
    extra = models.TextField(_("Informações Extras"),)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tax = models.DecimalField(
        _("Taxa de Entrega"), default=0, max_digits=10, decimal_places=2
    )
    delivery_time = models.CharField(
        _("Tempo de Espera"), max_length=20, blank=True, null=True
    )

    class Meta:
        ordering = ["-created"]
        verbose_name = _("Loja")
        verbose_name_plural = _("Lojas")
        index_together = (("id", "slug"),)
        db_table = "store"

    def __str__(self):
        return ("{} ({})").format(self.name, self.slug)

    def get_absolute_url(self):
        return reverse("shop:product_list", args=[self.slug])


class Category(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="categories_store",
        on_delete=models.CASCADE,
    )
    name = models.CharField(_("Nome"), max_length=200, db_index=True)
    slug = models.SlugField(_("URL"), max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ["name"]
        verbose_name = _("Categoria")
        verbose_name_plural = _("Categorias")
        db_table = "category"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            "shop:product_list_by_category", args=[self.store.slug, self.slug]
        )


class Product(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="products_store",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category,
        verbose_name=_("Categoria"),
        related_name="products_category",
        on_delete=models.CASCADE,
    )
    name = models.CharField(_("Nome"), max_length=200, db_index=True)
    slug = models.SlugField(_("URL"), max_length=200, db_index=True)
    image = models.ImageField(_("Foto"), upload_to="shop/products", blank=True)
    image_url = models.URLField(_("Endereço da Foto"), blank=True)
    description = models.TextField(_("Descrição"), blank=True)
    price = models.DecimalField(_("Preço"), max_digits=10, decimal_places=2)
    status = models.BooleanField(_("Status"), default=0, choices=PRODUCT_STATUS)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    unity = models.CharField(
        _("Unidade de Medida"), max_length=20, choices=UNITY_CHOICES, default="UNID"
    )

    class Meta:
        ordering = ["category", "name"]
        verbose_name = _("Produto")
        verbose_name_plural = _("Produtos")
        index_together = (("id", "slug"),)
        db_table = "product"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("shop:product_detail", args=[self.id, self.slug])

    def admin_image(self):
        if self.image:
            return format_html('<img src="{}" height="50" />'.format(self.image.url))
        elif self.image_url:
            return format_html('<img src="{}" height="50" />'.format(self.image_url))
        else:
            return "{}".format(_("Sem imagem"))

    admin_image.allow_tags = True
    admin_image.short_description = _("Imagem")

    # def save(self, *args, **kwargs):
    #     if self.image_url:
    #         img_temp = NamedTemporaryFile(delete=True)
    #         img_temp.write(urlopen(self.image_url).read())
    #         img_temp.flush()

    #         self.image.save(self.image, File(img_temp))


class Banner(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="banners_store",
        on_delete=models.CASCADE,
    )
    image = models.ImageField(_("Imagem"), upload_to="banners/%Y/%m/%d")
    title = models.CharField(_("Título"), max_length=100, blank=True)
    body = models.CharField(_("Texto do banner"), max_length=200, blank=True)
    button = models.CharField(_("Texto do botão"), max_length=20, blank=True)

    class Meta:
        ordering = ["title"]
        verbose_name = _("Banner")
        verbose_name_plural = _("Banners")
        db_table = "banner"

    def __str__(self):
        return self.image.url

    def admin_image(self):
        try:
            return format_html('<img src="{}" height="50" />'.format(self.image.url))
        except:
            return "{}".format(_("Sem imagem"))

    admin_image.allow_tags = True
    admin_image.short_description = _("Imagem")


class Schedule(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="schedules_store",
        on_delete=models.CASCADE,
    )
    day = models.IntegerField(_("Dia"), choices=WEEK_DAY_CHOICES)
    first_hour_start = models.TimeField(_("Primeiro Horário (Início)"), blank=True)
    first_hour_end = models.TimeField(_("Primeiro Horário (Fim)"), blank=True)
    second_hour_start = models.TimeField(_("Segundo Horário (Início)"), blank=True)
    second_hour_end = models.TimeField(_("Segundo Horário (Fim)"), blank=True)
    third_hour_start = models.TimeField(_("Terceiro Horário (Início)"), blank=True)
    third_hour_end = models.TimeField(_("Terceiro Horário (Fim)"), blank=True)

    class Meta:
        ordering = ["day"]
        verbose_name = _("Horário de Funcionamento")
        verbose_name_plural = _("Horários de Funcionamento")
        db_table = "schedule"

    def __str__(self):
        return ("{}: {}").format(self.store, self.get_day_display())

    def display_day_hour(self):
        hours = ""

        first_start = self.first_hour_start
        first_end = self.first_hour_end
        second_start = self.second_hour_start
        second_end = self.second_hour_end
        third_start = self.third_hour_start
        third_end = self.third_hour_end

        if first_start and first_end:
            hours = hours + ("{0} até {1}").format(first_start, first_end)
        if second_start and second_end:
            if first_start and first_end:
                hours = hours + ", "
            hours = hours + ("{0} até {1}").format(second_start, second_end)
        if third_start and third_end:
            if first_start and first_end:
                hours = hours + ", "
            hours = hours + ("{0} até {1}").format(third_start, third_end)

        return ("{} ({})").format(self.get_day_display(), hours)

    display_day_hour.allow_tags = True
    display_day_hour.short_description = _("Dia")
