import uuid

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


STATE_CHOICES = (
    ("AC", "Acre"),
    ("AL", "Alagoas"),
    ("AP", "Amapá"),
    ("AM", "Amazonas"),
    ("BA", "Bahia"),
    ("CE", "Ceará"),
    ("DF", "Distrito Federal"),
    ("ES", "Espírito Santo"),
    ("GO", "Goiás"),
    ("MA", "Maranhão"),
    ("MT", "Mato Grosso"),
    ("MS", "Mato Grosso do Sul"),
    ("MG", "Minas Gerais"),
    ("PA", "Pará"),
    ("PB", "Paraíba"),
    ("PR", "Paraná"),
    ("PE", "Pernambuco"),
    ("PI", "Piauí"),
    ("RJ", "Rio de Janeiro"),
    ("RN", "Rio Grande do Norte"),
    ("RS", "Rio Grande do Sul"),
    ("RO", "Rondônia"),
    ("RR", "Roraima"),
    ("SC", "Santa Catarina"),
    ("SP", "São Paulo"),
    ("SE", "Sergipe"),
    ("TO", "Tocantins"),
)

PRODUCT_STATUS = (
    (0, _("Ativo")),
    (1, _("Em Falta")),
    (2, _("Ocultar")),
)

WEEK_DAY_CHOICES = (
    (0, _("Domingo")),
    (1, _("Segunda")),
    (2, _("Terça")),
    (3, _("Quarta")),
    (4, _("Quinta")),
    (5, _("Sexta")),
    (6, _("Sabádo")),
)


class Store(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="store")
    logo = models.ImageField(_("Logo"), upload_to="shop/stores", blank=False)
    name = models.CharField(_("Nome da Loja"), max_length=100, db_index=True)
    slug = models.SlugField(_("URL"), max_length=100, db_index=True, unique=True)
    phone = models.CharField(_("Fone"), max_length=20)
    address = models.CharField(_("Endereço"), max_length=200)
    address2 = models.CharField(_("Complemento"), max_length=200, blank=True, null=True)
    postal_code = models.CharField(_("CEP"), max_length=20)
    city = models.CharField(_("Cidade"), max_length=100)
    state = models.CharField(_("UF"), max_length=2, choices=STATE_CHOICES, default="PE")
    extra = models.TextField(_("Informações Extras"),)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created"]
        verbose_name = _("Loja")
        verbose_name_plural = _("Lojas")
        index_together = (("id", "slug"),)
        db_table = "store"

    def __str__(self):
        return ("{} ({})").format(self.name, self.slug)

    def get_absolute_url(self):
        return reverse("shop:product_list", args=[self.slug])


class Category(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="categories_store",
        on_delete=models.CASCADE,
    )
    name = models.CharField(_("Nome"), max_length=200, db_index=True)
    slug = models.SlugField(_("URL"), max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ["name"]
        verbose_name = _("Categoria")
        verbose_name_plural = _("Categorias")
        db_table = "category"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            "shop:product_list_by_category", args=[self.store.slug, self.slug]
        )


class Product(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="products_store",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category,
        verbose_name=_("Categoria"),
        related_name="products_category",
        on_delete=models.CASCADE,
    )
    name = models.CharField(_("Nome"), max_length=200, db_index=True)
    slug = models.SlugField(_("URL"), max_length=200, db_index=True)
    image = models.ImageField(_("Foto"), upload_to="shop/products", blank=True)
    description = models.TextField(_("Descrição"), blank=True)
    price = models.DecimalField(_("Preço"), max_digits=10, decimal_places=2)
    status = models.BooleanField(_("Status"), default=0, choices=PRODUCT_STATUS)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["category", "name"]
        verbose_name = _("Produto")
        verbose_name_plural = _("Produtos")
        index_together = (("id", "slug"),)
        db_table = "product"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("shop:product_detail", args=[self.id, self.slug])

    def admin_image(self):
        try:
            return '<img src="{}" height="50" />'.format(self.image.url)
        except:
            return "{}".format(_("Sem imagem"))

    admin_image.allow_tags = True
    admin_image.short_description = _("Imagem")


class Banner(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="banners_store",
        on_delete=models.CASCADE,
    )
    image = models.ImageField(_("Imagem"), upload_to="banners/%Y/%m/%d")
    title = models.CharField(_("Título"), max_length=100, blank=True)
    body = models.CharField(_("Texto do banner"), max_length=200, blank=True)
    button = models.CharField(_("Texto do botão"), max_length=20, blank=True)

    class Meta:
        ordering = ["title"]
        verbose_name = _("Banner")
        verbose_name_plural = _("Banners")
        db_table = "banner"

    def __str__(self):
        return self.image.url

    def admin_image(self):
        try:
            return '<img src="{}" height="50" />'.format(self.image.url)
        except:
            return "{}".format(_("Sem imagem"))

    admin_image.allow_tags = True
    admin_image.short_description = _("Imagem")


class Schedule(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="schedules_store",
        on_delete=models.CASCADE,
    )
    day = models.IntegerField(_("Dia"), choices=WEEK_DAY_CHOICES)
    first_hour_start = models.TimeField(_("Primeiro Horário (Início)"), blank=True)
    first_hour_end = models.TimeField(_("Primeiro Horário (Fim)"), blank=True)
    second_hour_start = models.TimeField(_("Segundo Horário (Início)"), blank=True)
    second_hour_end = models.TimeField(_("Segundo Horário (Fim)"), blank=True)
    third_hour_start = models.TimeField(_("Terceiro Horário (Início)"), blank=True)
    third_hour_end = models.TimeField(_("Terceiro Horário (Fim)"), blank=True)

    class Meta:
        ordering = ["day"]
        verbose_name = _("Horário de Funcionamento")
        verbose_name_plural = _("Horários de Funcionamento")
        db_table = "schedule"

    def __str__(self):
        return ("{}: {}").format(self.store, self.get_day_display())

    def display_day_hour(self):
        hours = ""

        first_start = self.first_hour_start
        first_end = self.first_hour_end
        second_start = self.second_hour_start
        second_end = self.second_hour_end
        third_start = self.third_hour_start
        third_end = self.third_hour_end

        if first_start and first_end:
            hours = hours + ("{0} até {1}").format(first_start, first_end)
        if second_start and second_end:
            if first_start and first_end:
                hours = hours + ", "
            hours = hours + ("{0} até {1}").format(second_start, second_end)
        if third_start and third_end:
            if first_start and first_end:
                hours = hours + ", "
            hours = hours + ("{0} até {1}").format(third_start, third_end)

        return ("{} ({})").format(self.get_day_display(), hours)

    display_day_hour.allow_tags = True
    display_day_hour.short_description = _("Dia")
