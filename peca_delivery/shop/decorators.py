from django.db import connection
from django.core.exceptions import PermissionDenied


def is_public(function):
    def wrap(request, *args, **kwargs):
        if connection.schema_name != "public":
            return function(request, *args, **kwargs)
        raise PermissionDenied

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
