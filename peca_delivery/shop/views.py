from django.shortcuts import render, get_object_or_404, redirect
from django.db import connection
from django.core.paginator import Paginator

from peca_delivery.customers.models import Client
from peca_delivery.cart.cart import Cart
from peca_delivery.cart.forms import CartAddProductFormHidden

from .decorators import is_public
from .models import Store, Category, Product, Banner


@is_public
def store_list(request):
    client = get_object_or_404(Client, schema_name=connection.schema_name)

    stores = Store.objects.all()
    context = {"client": client, "stores": stores}

    if len(stores) > 1:
        return render(request, "shop/store/list.html", context)

    if stores:
        return redirect("shop:product_list", stores[0].slug)


@is_public
def product_list(request, store_slug=None, category_slug=None):
    category = None
    store = get_object_or_404(Store, slug=store_slug)
    search = request.GET.get("search")

    # remove car session how much store change
    cart = Cart(request)
    for x in cart:
        if not x["store"] == store.id:
            for key in list(request.session.keys()):
                del request.session[key]

    categories = Category.objects.filter(store=store.id)
    banners = Banner.objects.filter(store=store.id)

    if search:
        products = Product.objects.filter(
            store=store.id, status=0, name__icontains=search
        )
    else:
        products = Product.objects.filter(store=store.id, status=0)

    if category_slug:
        category = get_object_or_404(Category, store=store.id, slug=category_slug)
        products = products.filter(store=store.id, category=category)

    # pagination
    paginator = Paginator(products, 50)
    page = request.GET.get("page")
    products = paginator.get_page(page)

    cart_product_form = CartAddProductFormHidden()

    return render(
        request,
        "shop/product/list.html",
        {
            "store": store,
            "category": category,
            "categories": categories,
            "products": products,
            "banners": banners,
            "cart_product_form": cart_product_form,
        },
    )


# detalhes do produto desativado
# para ativar precisa finalizar o codigo
# def product_detail(request, product_id, slug):
#     product = get_object_or_404(Product,
#                                 id=product_id,
#                                 slug=slug,
#                                 status=0)
#     cart_product_form = CartAddProductForm()
#     return render(request,
#                   'shop/product/detail.html',
#                   {'product': product,
#                    'cart_product_form': cart_product_form})
