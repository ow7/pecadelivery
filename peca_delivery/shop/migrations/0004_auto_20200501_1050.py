# Generated by Django 2.2.12 on 2020-05-01 10:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("customers", "0002_client_logo"),
        ("shop", "0003_product_unity"),
    ]

    operations = [
        migrations.AddField(
            model_name="store",
            name="client",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="clients",
                to="customers.Client",
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="store",
            name="user",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="user",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
