from django.contrib import admin
from django.utils.html import format_html

from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ["product"]


class OrderAdmin(admin.ModelAdmin):
    list_filter = ["created", "status", "district", "payment"]
    list_display = [
        "id",
        "full_name",
        "phone",
        "address",
        "number",
        "district",
        "city",
        "status",
        "formatted_created",
    ]
    list_editable = ["status"]
    search_fields = [
        "id",
        "first_name",
        "last_name",
        "phone",
        "district",
        "city",
        "postal_code",
        "address",
    ]
    date_hierarchy = "created"
    inlines = [OrderItemInline]

    def full_name(self, obj):
        return "{} {}".format(obj.first_name, obj.last_name)

    full_name.allow_tags = True
    full_name.admin_order_field = "first_name"
    full_name.short_description = "Nome"

    def formatted_created(self, obj):
        """Format created date."""
        color = "white"
        color_text = "white"
        if obj.status == 0:
            # verificar
            color = "white"
            color_text = "black"
        elif obj.status == 1:
            # aprovado
            color = "orange"
            color_text = "white"
        elif obj.status == 2:
            # trânsito
            color = "blue"
            color_text = "white"
        elif obj.status == 3:
            # entregue
            color = "green"
            color_text = "white"
        else:
            # recusado
            color = "black"
            color_text = "white"
        return format_html(
            '<div style="background:{}; color: {}; '
            'text-align: center; padding: 4px;">{}</div>'.format(
                color, color_text, obj.created.strftime("%d/%m/%Y %H:%M")
            )
        )

    formatted_created.allow_tags = True
    formatted_created.admin_order_field = "created"
    formatted_created.short_description = "Data"

    def colored_status(self, obj):
        color = "white"
        color_text = "white"
        if obj.status == 0:
            color = "red"
            color_text = "white"
        elif obj.status == 1:
            color = "green"
            color_text = "white"
        elif obj.status == 2:
            color = "yellow"
            color_text = "black"
        elif obj.status == 3:
            color = "blue"
            color_text = "white"
        else:
            color = "grey"
            color_text = "white"
        return format_html(
            '<div style="background:{}; color: {}; '
            'text-align: center; padding: 4px;">{}</div>'.format(
                color, color_text, obj.get_status_display()
            )
        )

    colored_status.allow_tags = True
    colored_status.admin_order_field = "status"
    colored_status.short_description = "Status"


admin.site.register(Order, OrderAdmin)
