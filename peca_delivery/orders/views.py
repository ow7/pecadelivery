from django.shortcuts import render, redirect, get_object_or_404

# from django.contrib.auth.decorators import login_required

from peca_delivery.cart.cart import Cart
from peca_delivery.shop.models import Store

from .forms import OrderCreateForm
from .models import OrderItem, Order
from .task import order_created


# @login_required(login_url='/entrar/')
def order_create(request, store_slug):
    cart = Cart(request)
    store = get_object_or_404(Store, slug=store_slug)
    if cart:
        if request.method == "POST":
            form = OrderCreateForm(request.POST)
            if form.is_valid():
                order = form.save(commit=False)
                order.store = store
                order = form.save()
                for item in cart:
                    OrderItem.objects.create(
                        order=order,
                        product=item["product"],
                        price=item["price"],
                        quantity=item["quantity"],
                    )
                # clear the cart
                cart.clear()
                order_created(order.id)
                request.session["order_id"] = order.id
                # redirect to the payment
                # TODO: para demostracao modifiquei a finalizacao do
                # pagamento para a tela de done, mas para producao tera
                # que mudar
                # return redirect('payment:process')
                return render(
                    request, "payment/done.html", {"store": store, "order": order.id}
                )

        form = OrderCreateForm()

        context = {
            "cart": cart,
            "form": form,
            "store": store,
        }

        if request.user.is_authenticated:
            email = request.user.email
            # print(request.user)
            last_order = Order.objects.filter(email=email).order_by("-created")[:1]

            if last_order:
                context["order"] = last_order[0]

        return render(request, "orders/order/create.html", context)
    else:
        # TODO: criar uma rotina para quando o carrinho estiver vázio
        return redirect("shop:product_list", store.slug)
