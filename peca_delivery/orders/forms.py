from django import forms

from peca_delivery.payment.models import PaymentMethod

from .models import Order


class OrderCreateForm(forms.ModelForm):
    payment = forms.ModelChoiceField(
        queryset=PaymentMethod.objects.all(),
        empty_label=None,
        widget=forms.RadioSelect(attrs={"class": "custom-control-input"}),
    )
    no_change = forms.BooleanField(
        label="Não preciso de troco",
        initial=False,
        required=False,
        widget=forms.CheckboxInput(attrs={"class": "custom-control-input"}),
    )

    class Meta:
        model = Order
        fields = [
            "first_name",
            "last_name",
            "phone",
            "email",
            "postal_code",
            "address",
            "number",
            "address2",
            "district",
            "city",
            "state",
            "extra",
            "payment",
            "change",
            "no_change",
        ]
