from django.apps import AppConfig


class OrdersConfig(AppConfig):
    name = "peca_delivery.orders"
    verbose_name = "Pedidos"
