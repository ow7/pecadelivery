from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from peca_delivery.shop.models import Store, Product, STATE_CHOICES
from peca_delivery.payment.models import PaymentMethod


ORDER_STATUS = (
    (0, _("Verificar")),
    (1, _("Aprovado")),
    (2, _("Trânsito")),
    (3, _("Entregue")),
    (4, _("Recusado")),
)


class Order(models.Model):
    store = models.ForeignKey(
        Store,
        verbose_name=_("Loja"),
        related_name="orders_store",
        on_delete=models.CASCADE,
    )
    first_name = models.CharField(_("Primeiro nome"), max_length=50)
    last_name = models.CharField(_("Último nome"), max_length=50)
    email = models.EmailField(_("Email"))
    address = models.CharField(_("Endereço"), max_length=200)
    address2 = models.CharField(_("Complemento"), max_length=200, blank=True, null=True)
    postal_code = models.CharField(_("CEP"), max_length=20)
    city = models.CharField(_("Cidade"), max_length=100)
    state = models.CharField(_("UF"), max_length=2, choices=STATE_CHOICES, default="PE")
    extra = models.TextField(_("Observações"), blank=True)
    payment = models.ForeignKey(
        PaymentMethod,
        verbose_name=_("Forma de Pagamento"),
        related_name="orders_payment_method",
        on_delete=models.CASCADE,
    )
    change = models.DecimalField(
        _("Troco para"), max_digits=10, decimal_places=2, default=0, blank=True
    )
    no_change = models.BooleanField(
        _("Não preciso de troco"), default=False, blank=True
    )
    status = models.IntegerField(_("Status"), choices=ORDER_STATUS, default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    paid = models.BooleanField(default=False)
    phone = models.CharField(_("Telefone"), max_length=14)
    district = models.CharField(_("Bairro"), max_length=100)
    number = models.IntegerField(_("Número"))

    class Meta:
        ordering = ("created", "status")
        verbose_name = _("Pedido")
        verbose_name_plural = _("Pedidos")
        db_table = "order"

    def __str__(self):
        return "Pedido {}".format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.orders_order_item.all())

    def save(self, *args, **kwargs):
        if self.no_change:
            self.change = 0
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("app:orders", args=[self.store.slug])


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        verbose_name=_("Pedido"),
        related_name="orders_order_item",
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        verbose_name=_("Produto"),
        related_name="products_order_item",
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(_("Preço"), max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(_("Quantidade"), default=1)

    class Meta:
        verbose_name = _("Item Pedido")
        verbose_name_plural = _("Itens Pedidos")
        db_table = "order_item"

    def __str__(self):
        return "{}".format(self.id)

    def get_cost(self):
        return self.price * self.quantity
