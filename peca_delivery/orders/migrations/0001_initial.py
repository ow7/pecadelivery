# Generated by Django 3.0.5 on 2020-04-25 00:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('payment', '0001_initial'),
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50, verbose_name='Primeiro nome')),
                ('last_name', models.CharField(max_length=50, verbose_name='Último nome')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('address', models.CharField(max_length=200, verbose_name='Endereço')),
                ('address2', models.CharField(blank=True, max_length=200, null=True, verbose_name='Complemento')),
                ('postal_code', models.CharField(max_length=20, verbose_name='CEP')),
                ('city', models.CharField(max_length=100, verbose_name='Cidade')),
                ('state', models.CharField(choices=[('AC', 'Acre'), ('AL', 'Alagoas'), ('AP', 'Amapá'), ('AM', 'Amazonas'), ('BA', 'Bahia'), ('CE', 'Ceará'), ('DF', 'Distrito Federal'), ('ES', 'Espírito Santo'), ('GO', 'Goiás'), ('MA', 'Maranhão'), ('MT', 'Mato Grosso'), ('MS', 'Mato Grosso do Sul'), ('MG', 'Minas Gerais'), ('PA', 'Pará'), ('PB', 'Paraíba'), ('PR', 'Paraná'), ('PE', 'Pernambuco'), ('PI', 'Piauí'), ('RJ', 'Rio de Janeiro'), ('RN', 'Rio Grande do Norte'), ('RS', 'Rio Grande do Sul'), ('RO', 'Rondônia'), ('RR', 'Roraima'), ('SC', 'Santa Catarina'), ('SP', 'São Paulo'), ('SE', 'Sergipe'), ('TO', 'Tocantins')], default='PE', max_length=2, verbose_name='UF')),
                ('extra', models.TextField(blank=True, verbose_name='Observações')),
                ('change', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, verbose_name='Troco para')),
                ('no_change', models.BooleanField(blank=True, default=False, verbose_name='Não preciso de troco')),
                ('status', models.IntegerField(choices=[(0, 'Verificar'), (1, 'Aprovado'), (2, 'Trânsito'), (3, 'Entregue'), (4, 'Recusado')], default=0, verbose_name='Status')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('paid', models.BooleanField(default=False)),
                ('payment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders_payment_method', to='payment.PaymentMethod', verbose_name='Forma de Pagamento')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders_store', to='shop.Store', verbose_name='Loja')),
            ],
            options={
                'verbose_name': 'Pedido',
                'verbose_name_plural': 'Pedidos',
                'db_table': 'order',
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Preço')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Quantidade')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders_order_item', to='orders.Order', verbose_name='Pedido')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='products_order_item', to='shop.Product', verbose_name='Produto')),
            ],
            options={
                'verbose_name': 'Item Pedido',
                'verbose_name_plural': 'Itens Pedidos',
                'db_table': 'order_item',
            },
        ),
    ]
