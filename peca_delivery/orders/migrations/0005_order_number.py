# Generated by Django 3.0.5 on 2020-04-27 17:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0004_order_district'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='number',
            field=models.IntegerField(default=0, verbose_name='Número'),
            preserve_default=False,
        ),
    ]
