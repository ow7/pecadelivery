from django.apps import AppConfig


class AppConfig(AppConfig):
    name = "peca_delivery.app"
