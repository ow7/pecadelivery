from django.urls import path

from . import views

app_name = "app"
urlpatterns = [
    path("", views.orders, name="orders"),
    path("<int:order_pk>/", views.orders, name="order_items"),
    path("<int:order_pk>/action/", views.order_action, name="order_action"),
    path("<int:order_pk>/accept/", views.order_action, name="order_accept"),
    path("<int:order_pk>/reject/", views.order_action, name="order_reject"),
    path("<int:order_pk>/transit/", views.order_action, name="order_transit"),
    path("<int:order_pk>/delivered/", views.order_action, name="order_delivered"),
    path("dashboard/", views.dashboard, name="dashboard"),
]
