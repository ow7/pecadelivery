import datetime
import json

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.http import require_GET

from peca_delivery.shop.models import Store
from peca_delivery.orders.models import Order


def orders(request, store_slug=None, order_pk=None):
    today = datetime.datetime.today()
    delta = datetime.timedelta(days=1)
    yesterday = today - delta

    store = get_object_or_404(Store, slug=store_slug)
    all_orders = Order.objects.filter(store=store, created__gt=yesterday)[:50]

    context = {
        "store": store,
        "orders": all_orders,
    }

    if order_pk:
        order = get_object_or_404(
            Order, pk=order_pk, store=store, created__gt=yesterday
        )
    else:
        # adicionei esse order.pk para quando o order for null e
        # evitar o erro no ajax do orders.html
        order = {}
        order["pk"] = 0

    context["order"] = order

    return render(request, "app/orders.html", context)


def dashboard(request, store_slug=None):
    store = get_object_or_404(Store, slug=store_slug)

    context = {
        "store": store,
    }

    return render(request, "app/dashboard.html", context)


@require_GET
def order_action(request, store_slug=None, order_pk=None):
    if request.method == "GET":
        # para /accept/
        # path = request.path.split('/')[:-1]
        # action = path[-1]
        # para ?a=accept
        path = request.GET.get("a")
        action = path

        order = Order.objects.get(pk=order_pk)

        # FIXME: melhorar a validacao
        if action == "accept":
            if order.status == 0:
                order.status = 1
        elif action == "transit":
            if order.status == 1:
                order.status = 2
        elif action == "delivered":
            if order.status == 2:
                order.status = 3
        else:
            # reject
            if order.status == 0:
                order.status = 4

        order.save()

        data = {}
        data["success"] = "Sucesso!"

        return HttpResponse(json.dumps(data), content_type="application/json")

    return HttpResponse(
        json.dumps({"status": "error"}), content_type="application/json"
    )
