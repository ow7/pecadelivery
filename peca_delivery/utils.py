from django.db import connection
from storages.backends.s3boto3 import S3Boto3Storage


StaticRootS3Boto3Storage = lambda: S3Boto3Storage(location="static")
# MediaRootS3Boto3Storage = lambda: S3Boto3Storage(location='media')


class MediaRootS3Boto3Storage(S3Boto3Storage):
    @property
    def location(self):
        path = "media"
        if hasattr(connection, "schema_name"):
            path = path + "/" + connection.schema_name
        return path

    @location.setter
    def location(self, value):
        pass
