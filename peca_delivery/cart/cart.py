from decimal import Decimal

from django.conf import settings

from peca_delivery.shop.models import Product


class Cart(object):
    def __init__(self, request):
        """
        Initialize the cart.
        """
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            # save an empty cart in the session
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def add(self, product, quantity=1, update_quantity=False):
        """
        Add a product to the cart or update its quantity.
        """
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {
                "quantity": 0,
                "price": str(product.price),
                "store": product.store.id,
            }
        if update_quantity:
            self.cart[product_id]["quantity"] = quantity
        else:
            self.cart[product_id]["quantity"] += quantity
        self.save()

    def save(self):
        # update the session cart
        self.session[settings.CART_SESSION_ID] = self.cart
        # mark the session as "modified" to make sure it is saved
        self.session.modified = True

    def remove(self, product):
        """
        Remove a product from the cart.
        """
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def __iter__(self):
        """
        Iterate over the items in the cart and get the products
        from the database.
        """
        product_ids = self.cart.keys()
        # get the product objects and add them to the cart
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]["product"] = product

        for item in self.cart.values():
            item["price"] = Decimal(item["price"])
            item["total_price"] = item["price"] * item["quantity"]
            yield item

    def __len__(self):
        """
        Count all items in the cart.
        """
        # django-shop-tutorial-master/shop/templates/shop/base.html
        # {% with total_items=cart|length %}
        return sum(item["quantity"] for item in self.cart.values())

    def get_cart(self, product):
        data = {}
        product_id = str(product.id)
        data["product_id"] = product.id
        data["product_name"] = product.name
        data["product_unity"] = product.unity
        data["quantity"] = self.cart[product_id]["quantity"]
        data["price"] = str(self.cart[product_id]["price"])
        data["total_price"] = sum(
            Decimal(data["price"]) * data["quantity"] for item in self.cart[product_id]
        )
        data["total_items"] = sum(data["quantity"] for item in self.cart[product_id])

        return data

        # if product_id not in self.cart:
        #     self.cart[product_id] = {'quantity': 0,
        #                              'price': str(product.price)}

        # return self.cart

    def get_total_price(self):
        return sum(
            Decimal(item["price"]) * item["quantity"] for item in self.cart.values()
        )

    def clear(self):
        # remove cart from session
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True
