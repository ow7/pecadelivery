from django.apps import AppConfig


class CartConfig(AppConfig):
    name = "peca_delivery.cart"
    verbose_name = "Carrinho"
