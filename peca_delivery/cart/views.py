import json

from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST

from peca_delivery.shop.models import Product

from .cart import Cart
from .forms import CartAddProductForm


# @require_POST
# def cart_add(request, product_id):
#     cart = Cart(request)
#     product = get_object_or_404(Product, id=product_id)
#     form = CartAddProductForm(request.POST)
#     if form.is_valid():
#         cd = form.cleaned_data
#         cart.add(product=product,
#                  quantity=cd['quantity'],
#                  update_quantity=cd['update'])
#     return redirect('cart:cart_detail')


@require_POST
def cart_add(request, product_id):
    if request.method == "POST":
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        form = CartAddProductForm(request.POST)
        if form.is_valid():
            c_data = form.cleaned_data
            cart.add(
                product=product,
                quantity=c_data["quantity"],
                update_quantity=c_data["update"],
            )

            product_id = str(product.id)
            data = {}
            data["product_id"] = product.id
            data["product_name"] = product.name
            data["product_unity"] = product.unity
            data["quantity"] = cart.cart[product_id]["quantity"]
            data["price"] = cart.cart[product_id]["price"]

            total_price = cart.get_total_price()
            data["total_price"] = str(total_price)

            total_items = cart.__len__()
            data["total_items"] = str(total_items)

            # return HttpResponse(data, content_type="application/json")
            return HttpResponse(json.dumps(data), content_type="application/json")

    return HttpResponse(
        json.dumps({"status": "error"}), content_type="application/json"
    )


def cart_remove(request, product_id):
    if request.method == "GET":
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        cart.remove(product)
        data = {}
        data["product_id"] = product_id

        total_price = cart.get_total_price()
        data["total_price"] = str(total_price)

        total_items = cart.__len__()
        data["total_items"] = str(total_items)

        return HttpResponse(json.dumps(data), content_type="application/json")

    return HttpResponse(
        json.dumps({"status": "error"}), content_type="application/json"
    )
    # return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item["update_quantity_form"] = CartAddProductForm(
            initial={"quantity": item["quantity"], "update": True}
        )
    return render(request, "cart/detail.html", {"cart": cart})
