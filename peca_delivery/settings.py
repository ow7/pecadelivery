import os

from decouple import Csv, config
from dj_database_url import parse


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", default=False, cast=bool)

ALLOWED_HOSTS = config("ALLOWED_HOSTS", cast=Csv())

DEFAULT_FROM_EMAIL = config("DEFAULT_FROM_EMAIL")

SITE_ID = 1

DOMAIN = config("DOMAIN")

SHARED_APPS = [
    "django_tenants",  # mandatory, should always be before any django app
    "peca_delivery.customers",  # you must list the app where your tenant model resides in
    "django.contrib.contenttypes",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "storages",
    "django_extensions",
    "bootstrap3",
    "bootstrap4",
    "debug_toolbar",
]

TENANT_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "peca_delivery.shop",
    "peca_delivery.cart",
    "peca_delivery.payment",
    "peca_delivery.orders",
    "peca_delivery.app",
    "paypal.standard.ipn",
]

# INSTALLED_APPS = ["peca_delivery.apps.SuitConfig"]

INSTALLED_APPS = list(SHARED_APPS) + [
    app for app in TENANT_APPS if app not in SHARED_APPS
]

TENANT_MODEL = "customers.Client"

TENANT_DOMAIN_MODEL = "customers.Domain"

MIDDLEWARE = [
    "django_tenants.middleware.TenantMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "peca_delivery.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "peca_delivery.cart.context_processors.cart",
            ],
            # 'loaders': [
            #     ('django_tenants.template_loaders.CachedLoader', (
            #     'django_tenants.template_loaders.FilesystemLoader',
            #     'django.template.loaders.filesystem.Loader',
            #     'django.template.loaders.app_directories.Loader')),
            # ],
        },
    },
]

# MULTITENANT_TEMPLATE_DIRS = [
#     [os.path.join(BASE_DIR, 'tenant_templates')],
# ]

WSGI_APPLICATION = "peca_delivery.wsgi.application"

# Email configuration
EMAIL_BACKEND = config("EMAIL_BACKEND")
EMAIL_HOST = config("EMAIL_HOST")
EMAIL_PORT = config("EMAIL_PORT", cast=int)
EMAIL_USE_TLS = config("EMAIL_USE_TLS", cast=bool)
EMAIL_HOST_USER = config("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = config("EMAIL_HOST_PASSWORD")

# django-paypal settings
PAYPAL_RECEIVER_EMAIL = "kleberr@msn.com"
PAYPAL_TEST = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {}
FALLBACK_DATABASE = "sqlite:///db.sqlite3"
DATABASES["default"] = parse(config("DATABASE_URL", default=FALLBACK_DATABASE))
DATABASES["default"]["ENGINE"] = "django_tenants.postgresql_backend"

DATABASE_ROUTERS = [
    "django_tenants.routers.TenantSyncRouter",
]

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'verbose': {
#             'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
#         },
#         'simple': {
#             'format': '%(levelname)s %(message)s'
#         },
#     },
#     # 'filters': {
#     #     'special': {
#     #         '()': 'project.logging.SpecialFilter',
#     #         'foo': 'bar',
#     #     },
#     #     'require_debug_true': {
#     #         '()': 'django.utils.log.RequireDebugTrue',
#     #     },
#     # },
#     'handlers': {
#         'console': {
#             'level': 'DEBUG',
#             # 'filters': ['require_debug_true'],
#             'class': 'logging.StreamHandler',
#             'formatter': 'verbose'
#         },
#         # 'file': {
#         #     'level': 'DEBUG',
#         #     'class': 'logging.FileHandler',
#         #     'filename': 'mylog_test.log',
#         #     'formatter': 'verbose'
#         # },

#         # 'mail_admins': {
#         #     'level': 'ERROR',
#         #     'class': 'django.utils.log.AdminEmailHandler',
#         #     'filters': ['special']
#         # }
#     },
#     'loggers': {
#         # 'django': {
#         #     'handlers': ['console'],  # console or file
#         #     'propagate': True,
#         #     'level': 'DEBUG',
#         # },
#         # 'django.request': {
#         #     'handlers': ['console'],
#         #     'level': 'DEBUG',
#         #     'propagate': False,
#         # },
#         # For performance reasons, SQL logging is only enabled when settings.DEBUG is set to True
#         # ref. https://docs.djangoproject.com/en/1.11/topics/logging/#django-db-backends
#         'django.db.backends': {
#             'handlers': ['console'],
#             'propagate': False,
#             'level': 'DEBUG',
#         },

#     }
# }

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = "pt-br"

TIME_ZONE = "Brazil/East"

USE_I18N = True

USE_L10N = True

USE_TZ = False

# AMAZON S3
DEFAULT_FILE_STORAGE = config("DEFAULT_FILE_STORAGE")
AWS_QUERYSTRING_AUTH = False
AWS_ACCESS_KEY_ID = config("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = config("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = config("AWS_STORAGE_BUCKET_NAME")
AWS_REGION_NAME = config("AWS_REGION_NAME")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'static')
# ]

if DEBUG:
    MEDIA_URL = "/media/"
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
else:
    MEDIA_URL = "https://{}.s3.amazonaws.com/media/".format(AWS_STORAGE_BUCKET_NAME)
    MEDIA_ROOT = "media"
    # THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

CART_SESSION_ID = "cart"

LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

ADMIN_SITE_HEADER = "Peça Delivery"

# SSl/TLS
SECURE_SSL_REDIRECT = config("SECURE_SSL_REDIRECT", default=False, cast=bool)
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", config("PROTOCOL"))
SESSION_COOKIE_SECURE = config("SESSION_COOKIE_SECURE", default=False, cast=bool)
CSRF_COOKIE_SECURE = config("CSRF_COOKIE_SECURE", default=False, cast=bool)

# DEBUG TOOLBAR
INTERNAL_IPS = ("127.0.0.1",)

DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
}

